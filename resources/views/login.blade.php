<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $page_title }} - {{ $site_title }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Muhammad Sumon Molla Selim (AAPBD)" name="author">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/pages/css/login.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/layouts/layout2/css/layout.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/layouts/layout2/css/themes/blue.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/custom.css') }}" rel="stylesheet">
</head>

<body class="login">
<div class="menu-toggler sidebar-toggler">
</div>

<div class="logo">
    <a href="{{ url('admin') }}">
        Sloppy2ndz
    </a>
</div>

<div class="content">

    <form action="{{ url('admin') }}" method="post">
        <h3 class="form-title">Sign In</h3>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session()->has('message'))
            <div class="alert alert-danger">
                <ul>
                    <li>{{ session('message') }}</li>
                </ul>
            </div>
        @endif

        <div class="form-group ">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input autocomplete="off" class="form-control form-control-solid placeholder-no-fix" type="text"
                   name="email" placeholder="email">
        </div>

        <div class="form-group ">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input autocomplete="off" class="form-control form-control-solid placeholder-no-fix pword" type="password"
                   name="password" placeholder="Password">
        </div>

        {!! csrf_field() !!}

        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase">Login</button>
        </div>

    </form>
</div>


<div class="copyright">
    Copyright &copy; {{ $site_title }} {{ date('Y') }}
</div>

<script src="{{  asset('assets/global/plugins/jquery.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jquery-migrate.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jquery.blockui.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/js.cookie.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>

<script src="{{  asset('assets/global/scripts/app.min.js') }}"></script>

<script src="{{  asset('assets/layouts/layout2/scripts/layout.min.js') }}"></script>
<script src="{{  asset('assets/layouts/layout2/scripts/demo.min.js') }}"></script>
<script src="{{  asset('assets/pages/scripts/login.js') }}"></script>

<script>
    jQuery(document).ready(function () {
        Layout.init(); // init current layout
    });
</script>

</body>
</html>
