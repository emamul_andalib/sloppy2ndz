<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $page_title }} - {{ $site_title }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Muhammad Sumon Molla Selim (AAPBD)" name="author">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/layouts/layout2/css/layout.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/layouts/layout2/css/themes/blue.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/custom.css') }}" rel="stylesheet">
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

<!-- BEGIN HEADER -->
@include('partials/_header')
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    @include('partials/_sidebar')
    <!-- END SIDEBAR -->
    @yield('main')
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
@include('partials._footer')
<!-- END FOOTER -->

<script src="{{  asset('assets/global/plugins/jquery.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/js.cookie.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jquery.blockui.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>

<script src="{{  asset('assets/global/plugins/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{  asset('assets/global/plugins/morris/morris.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/morris/raphael-min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amcharts/amcharts.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amcharts/serial.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amcharts/pie.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amcharts/radar.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amcharts/themes/light.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/ammap/ammap.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') }}"></script>
<script src="{{  asset('assets/global/plugins/amcharts/amstockcharts/amstock.js') }}"></script>
<script src="{{  asset('assets/global/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/flot/jquery.flot.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jquery.sparkline.min.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<script src="{{  asset('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}"></script>

<script src="{{  asset('assets/global/scripts/app.min.js') }}"></script>
<script src="{{  asset('assets/pages/scripts/dashboard.min.js') }}"></script>

<script src="{{  asset('assets/layouts/layout2/scripts/layout.min.js') }}"></script>
<script src="{{  asset('assets/layouts/layout2/scripts/demo.min.js') }}"></script>

</body>
</html>