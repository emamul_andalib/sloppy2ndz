<?php

/*
|--------------------------------------------------------------------------
| Sloppy2ndz Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', function () {
    return 'Sloppy2ndz API v1';
});

/**
 * Admin Routes
 */
Route::group(['prefix' => 'admin'], function () {
    /*
     * before authorization
     */
    Route::get('/', [
        'as' => 'login',
        'uses' => 'AuthController@login'
    ]);
    Route::post('/', 'AuthController@processLogin');

    /*
     * after authorization
     */
    Route::get('dashboard', [
        'as' => 'dashboard',
        'uses' => 'AdminController@dashboard',
    ]);
    Route::get('logout', [
        'as' => 'logout',
        'uses' => 'AuthController@logout',
    ]);

    // Users
    Route::resource('users', 'UsersController');

    // Products
    Route::resource('products', 'ProductsController');
});

/**
 * API Routes
 * Version: 1.0
 */
Route::group(['prefix' => 'api/v1'], function () {
    Route::post('register', 'ApiController@register');
    Route::post('login', 'ApiController@login');
    Route::post('login/facebook', 'ApiController@loginViaFacebook');
    Route::post('login/instagram', 'ApiController@loginViaInstagram');

    // routes for authenticated users only
    Route::group(['middleware' => 'jwt.auth'], function () {
        // Settings
        Route::post('edit-profile', 'ApiController@editProfile');
        Route::post('edit-billing', 'ApiController@editBilling');
        Route::post('edit-paypal', 'ApiController@editPaypal');
        Route::post('edit-bank', 'ApiController@editBank');
        Route::post('edit-currency', 'ApiController@editCurrency');

        // Profile
        Route::get('profile', 'ApiController@userProfile');

        // Product Category
        Route::post('add-category', 'ApiController@addCategory');
        Route::get('categories', 'ApiController@listCategories');
        Route::get('category', 'ApiController@categoryDetails');

        // Product
        Route::post('add-product', 'ApiController@addProduct');
        Route::get('product', 'ApiController@productDetails');
        Route::post('add-comment', 'ApiController@addComment');
        Route::get('comments', 'ApiController@listComments');
    });
});