<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;

class AdminController extends Controller
{
    /**
     * Set the site title and admin authorization check.
     */
    function __construct()
    {
        $this->site_title = 'Sloppy2ndz';
        $this->middleware('auth');
    }

    /**
     * View the dashboard.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        $data = [];
        $data['site_title'] = $this->site_title;
        $data['page_title'] = 'Dashboard';
        $data['users'] = User::where('role', 'user')->count();
        $data['products'] = Product::where('status', 'published')->count();

        return view('dashboard', $data);
    }
}
