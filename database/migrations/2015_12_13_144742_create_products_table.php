<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->text('photos');
            $table->integer('category_id')->unsigned();
            $table->string('brand', 64);
            $table->string('size', 64);
            $table->enum('new_tag', ['yes', 'no'])->default('no');
            $table->float('original_price');
            $table->float('listing_price');
            $table->text('hashtags');
            $table->enum('delivery_meet_in_person', ['yes', 'no'])->default('no');
            $table->enum('delivery_shipping', ['yes', 'no'])->default('no');
            $table->enum('delivery_international_shipping', ['yes', 'no'])->default('no');
            $table->enum('status', ['pending', 'published', 'rejected'])->default('published');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
