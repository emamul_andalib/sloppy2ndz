<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 32);
            $table->string('last_name', 32);
            $table->string('email', 64)->unique();
            $table->string('username', 32)->unique();
            $table->string('password', 80);
            $table->string('gender', 16);
            $table->enum('role', ['admin', 'user'])->default('user');
            $table->string('country', 64);
            $table->text('profile_image')->nullable();
            $table->text('short_bio')->nullable();
            $table->string('facebook', 32)->nullable();
            $table->string('instagram', 32)->nullable();
            $table->string('paypal_email', 32)->nullable();
            $table->text('bank_account')->nullable();
            $table->string('currency', 8)->nullable();
            $table->string('billing_type', 16)->nullable();
            $table->string('billing_name', 48)->nullable();
            $table->text('billing_address')->nullable();
            $table->string('billing_city', 32)->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_zip')->nullable();
            $table->string('billing_country')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
